/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplos.recursividad;

/**
 *
 * @author Gibran Ojeda
 */
public class ejerciciosRecursividad {
    
    public double logEIterativo() {
        double enl, delta, fact;
        int n;
        n = 1;
        enl = delta = fact = 1.0;
        do {
            enl += delta;
            n++;
            fact *= n;
            delta = 1.0 / fact;
        } while (enl != enl + delta);
        return enl;
    } //Problema 1

    public double logERecursivo(double enl, double delta, double fact, int n) {
        if (enl == enl + delta) //Caso base
        {
            return enl;
        } else {
            enl += delta;
            fact *= (n += 1);
            delta = 1.0 / fact;
            return logERecursivo(enl, delta, fact, n);
        }
    } //Problema 1

    public long factorialRecursivo(long n) {
        if (n == 0 || n == 1) {
            return 1;
        } else {
            return n * factorialRecursivo(--n);
        }
    } //Problema 2

    public long f(int n) {
        if (n == 0 || n == 1) {
            return 1;
        } else {
            return 3 * f(n - 2) + 2 * f(n - 1);
        }
    } //Problema 3

    public int vocales(String s, int posicion) {
        s= s.toLowerCase();
        int pos=0;
        pos+=posicion;
        if (pos < 0 || pos >= s.length()) {
            return 0;
        } else if (s.charAt(pos) == 'a' || s.charAt(pos) == 'e' || s.charAt(pos) == 'i' || s.charAt(pos) == 'o' || s.charAt(pos) == 'u') {
            return 1 + vocales(s, pos += 1);
        } else {
            return 0 + vocales(s, pos += 1);
        }
    } //Problema 4

    public int productoN(int x, int y) {
        if (x == 0 || y == 0) {
            return 0;
        }
        return x + productoN(x, y - 1);
    } //Problema 5 - A
    
    public int leyAckermann(int m, int n){
        if(m==0)
            return n+1;
        else if(n==0)
            return leyAckermann(m-1,1);
        else
            return leyAckermann(m-1,leyAckermann(m,n-1));
    } //Problema 7
    
    public long function(int n){
        if(n==0 || n==1)
            return 1;
        else if(n%2==0)
            return 2+function(n-1);
        else
            return 3+function(n-2);
    } //Problema 8
    
    public int funcion(int n){
        if(n==0)
            return 1;
        else if(n==1)
            return 2;
        else
            return 2*funcion(n-2)+funcion(n-1);
    } //Problema 9

    public static void main(String[] args) {
        ejerciciosRecursividad eR = new ejerciciosRecursividad();
        System.out.println("Problema 5.1 - LogE Recursivo: " + eR.logERecursivo(1.0, 1.0, 1.0, 1));
        System.out.println("Problema 5.1 - LogE Iterativo: " + eR.logEIterativo());
        System.out.println("Problema 5.2 - Factorial Recursivo: " + eR.factorialRecursivo(9));
        System.out.println("Problema 5.3 - Cuál será el resultado cuando f(5): " + eR.f(5));
        System.out.println("Problema 5.4 - Calcular el número de vocales en una cadena; cuando vocales(\"Surisareth Alicia\"): "+eR.vocales("Surisareth Alicia", 0));
        System.out.println("Problema 5.5 - A) Producto de dos números naturales recursivo; cuando f(3,5): "+eR.productoN(3, 5));
        System.out.println("Problema 5.7 - Método recursivo que calcula la función Ackermann; cuando leyAckermann(2,4): "+eR.leyAckermann(2,4));
        System.out.println("Problema 5.8 - Secuencia numérica generada cuando f(8): "+eR.function(8));
        System.out.println("Problema 5.9 - ¿Cuál es la secuencia generada? Respuesta: Potencias de 2. Cuando f(5)=2^5, es decir: "+eR.funcion(5));
    }
}
